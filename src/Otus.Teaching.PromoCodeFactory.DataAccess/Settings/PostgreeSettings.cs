﻿namespace  Otus.Teaching.PromoCodeFactory.DataAccess.Settings
{
    public class PostgresSettings
    {
        public string Server { get; set; }
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Database { get; set; }

        public string GetConnectionString()
        {
            return $"Server={Server};Port={Port};Username={UserName};Password={Password};Database={Database}";
        }
    }
}